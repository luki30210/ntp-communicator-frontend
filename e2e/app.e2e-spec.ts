import { NtpCommunicatorPage } from './app.po';

describe('ntp-communicator App', () => {
  let page: NtpCommunicatorPage;

  beforeEach(() => {
    page = new NtpCommunicatorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
