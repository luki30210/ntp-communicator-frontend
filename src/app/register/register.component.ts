import { Component, OnInit } from '@angular/core';
import { FormGroup, /*FormControl*/ FormBuilder, Validators } from '@angular/forms';

import { User } from '../model/user';
import { UserService } from '../service/user.service';

@Component({
	templateUrl: './register.component.html',
	styleUrls: ['../login/login.component.css'],
	providers: [UserService]
})
export class RegisterComponent implements OnInit {

	public registerForm: FormGroup;
    public userAdded: boolean;
    public statusCode: number;

    constructor(private _formBuilder: FormBuilder, private _userService: UserService) { }

	ngOnInit() {
		this.userAdded = false;
		this.statusCode = 0;

		//let regExp = new RegExp('\^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}\$');
		let regExp = new RegExp('\^[0-3]?[0-9]-[0-1]?[0-9]-[0-9]{4}\$');

        this.registerForm = this._formBuilder.group({
            firstName: [null, [Validators.required, Validators.minLength(4)]],
			lastName: [null, [Validators.required, Validators.minLength(4)]],
			login: [null, [Validators.required, Validators.minLength(4)]],
			email: [null, [Validators.required, Validators.email]],
            dateOfBirth: [null, [Validators.required, Validators.pattern(regExp)]],
			password1: [null, [Validators.required]],
			password2: [null, [Validators.required]]
        });

	}

	onSubmit() {
		let formData = this.registerForm.value;

		let user = new User({
			"id": 0,
			"login": formData.login,
			"password": formData.password1,
			"firstName": formData.firstName,
			"lastName": formData.lastName,
			"dateOfBirth": formData.dateOfBirth,
			"email": formData.email
		});

		this._userService.addUser(user).subscribe(
			data => {
				this.userAdded = true;
				this.statusCode = data.status;
			},
			error => {
				this.userAdded = false;
				this.statusCode = error.status;
			});

	}

}
