import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { User } from '../model/user';
import { Resources } from '../service/resources';

@Injectable()
export class UserService {

	constructor(private _http: Http) { }

	getUser(id: number) {
		return this._http.get(Resources._URL + '/user/' + id)
			.map((response: Response) => response.json())
			.catch(this._errorHandler);
	}

	getUserByLogin(login: string) {
		return this._http.get(Resources._URL + '/user/login/' + login)
			.map((response: Response) => response.json())
			.catch(this._errorHandler);
	}

	getUserLogged() {
		return this._http.get(Resources._URL + '/user/login/meok')
			.map((response: Response) => response.json())
			.catch(this._errorHandler);
	}

	getUsers(): Observable<User[]> {
		return this._http.get(Resources._URL + '/user')
			.map((response: Response) => response.json())
			.catch(this._errorHandler);
	}

	addUser(user: User) {
		let body = JSON.stringify(user);
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		return this._http.post(Resources._URL + '/user', body, options);
	}

	private _errorHandler(error: Response) {
		console.error(error);
		return Observable.throw(error || "Server error");
	}

	private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }

}
