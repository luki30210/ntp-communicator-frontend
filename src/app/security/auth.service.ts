import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { User } from '../model/user';
import { UserService } from '../service/user.service';

@Injectable()
export class AuthService {

	isLoggedIn = false;
	redirectUrl: string;	// store the URL so we can redirect after logging in

	constructor(private _userService: UserService) { }

	login(): Observable<boolean> {
		return Observable.of(true).delay(1000).do(
			val => {
				this.isLoggedIn = true;
			}
		);
	}

	logout(): void {
		this.isLoggedIn = false;
	}
}
