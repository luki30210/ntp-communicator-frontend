import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'

import { AuthGuard } from '../security/auth.guard';

import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { ConversationComponent } from '../conversation/conversation.component';

import { UserService } from '../service/user.service';
import { AuthService } from '../security/auth.service';


const routes: Routes = [
    //{ path: '', redirectTo: '/', pathMatch: 'full' },
    {
		path: '', component: HomeComponent, canActivate: [AuthGuard], children: [
			{ path: 'conversation/:userId', component: ConversationComponent, pathMatch: 'full'}
		]
	},
    { path: 'login', component: LoginComponent, pathMatch: 'full' },
	{ path: 'register', component: RegisterComponent, pathMatch: 'full'},
	{ path: '**', redirectTo: '' },
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [
		RouterModule
	],
	providers: [AuthGuard, AuthService, UserService]
})
export class AppRoutingModule { }

export const routingComponents = [
    HomeComponent,
	LoginComponent,
	RegisterComponent,
	ConversationComponent
];
