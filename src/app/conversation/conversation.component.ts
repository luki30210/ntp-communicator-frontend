import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { User } from '../model/user';
import { UserService } from '../service/user.service';
import { Message } from '../model/message';
import { MessageService } from '../service/message.service';

@Component({
	selector: 'app-conversation',
	templateUrl: './conversation.component.html',
	styleUrls: ['./conversation.component.css'],
	providers: [UserService, MessageService]
})
export class ConversationComponent implements OnInit {

	user: User;

	constructor(private _userService: UserService, private _messageService: MessageService, private _aRoute: ActivatedRoute) { }

	ngOnInit() {
		this._aRoute.params.subscribe((params: Params) => {
			this._userService.getUser(parseInt(params['userId'])).subscribe(
				user => this.user = user,
				error => console.log(error)
			)
		});
	}

}
