export class Message {

	id: number;
	sentBy: number;
	sentTo: number;
	isReceived: boolean;
	content: string;
	timestamp: string; //Date

	constructor(msg: { "id": number, "sentBy": number, "sentTo": number, "isReceived": boolean, "content": string, "timestamp": string }) {
		this.id = msg.id;
		this.sentBy = msg.sentBy;
		this.sentTo = msg.sentTo;
		this.isReceived = msg.isReceived;
		this.timestamp = msg.timestamp;
	}

}
