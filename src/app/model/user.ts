export class User {

	/*private*/ id: number;
	/*private*/ firstName: string;
	/*private*/ lastName: string;
	/*private*/ dateOfBirth: string;
	/*private*/ email: string;

	login: string;
	password: string;

	constructor (userInfo: { "id": number, "login": string, "password": string, "firstName": string, "lastName": string, "dateOfBirth": string, "email": string }) {
		this.id = userInfo.id;
		this.firstName = userInfo.firstName;
		this.lastName = userInfo.lastName;
		this.dateOfBirth = userInfo.dateOfBirth;
		this.email = userInfo.email;
		this.login = userInfo.login;
		this.password = userInfo.password;
	}

	/*
	constructor(id: number, firstName: string, lastName: string, dateOfBirth: number, email: string) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.email = email;
	}
	*/

	/*
    constructor(
		private id: number,
		private firstName: string,
		private lastName: string,
		private dateOfBirth: number,
		private email: string
	) { }
	*/

	/*
	public getId(): number {
		return this.id;
	}

	public setId(newId: number) {
		this.id = newId;
	}

	public getFirstName() {
		return this.firstName;
	}

	public setFirstName(newId: number) {
		this.id = newId;
	}

	public getLastName() {
		return this.lastName;
	}

	public setLastName(newFirstName: string) {
		this.firstName = newFirstName;
	}

	public getDateOfBirt(): string {
		return this.dateOfBirth;
	}

	public setDateOfBirth(newDateOfBirt: string) {
		this.dateOfBirth = newDateOfBirt;
	}

	public getEmail(): string {
		return this.email;
	}

	public setEmail(newEmail: string) {
		this.email = newEmail;
	}
	*/

}
