import { Component, OnInit } from '@angular/core';

import { User } from '../model/user';
import { UserService } from '../service/user.service';

@Component({
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
	providers: [UserService]
})
export class HomeComponent implements OnInit {

	public users: User[] = [];
	public loggedUser: User;
	public errorMsg: string;
	public authenticated: boolean;

	constructor(private _userService: UserService) { }

	ngOnInit() {
		this.authenticated = false;
		this.getUsersList();
	}

	getUsersList() {
		this._userService.getUsers().subscribe(
			users => this.users = users,
			error => {
				console.log(error);
				this.errorMsg = error;
			}
		);
	}

}
