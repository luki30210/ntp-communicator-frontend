import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { User } from '../model/user';
import { UserService } from '../service/user.service';
import { AuthenticationService } from '../security/authentication.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css'],
	providers: [UserService, AuthenticationService]
})
export class LoginComponent implements OnInit {

	//public user: User;
    private errorMsg: string;
	public loginForm: FormGroup;
    //public userLogged: boolean;
    //private statusCode: number;
    private loading = false;
	private returnUrl: string;

    constructor(
		private _userService: UserService,
		private _formBuilder: FormBuilder,
		private _route: ActivatedRoute,
        private _router: Router,
        private _authenticationService: AuthenticationService
	) { }

    ngOnInit() {
		this._authenticationService.logout();
		//this.statusCode = 0;
		this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';

		this.loginForm = this._formBuilder.group({
			login: [null, [Validators.required, Validators.minLength(4)]],
			password: [null, [Validators.required]]
        });
	}

	onSubmit() {
		/*let formData = this.loginForm.value;
		this._userService.getUserByLogin(formData.login).subscribe(
			user => {
				this.user = user; //Bind to view
				this.userLogged = true;
				console.log(this.user);
			},
			err => console.log(err)
		);*/

		if (this.loginForm.valid) {
			this.login();
		}
    }

	login() {
        this.loading = true;
        this._authenticationService.login(this.loginForm.value.login, this.loginForm.value.password)
            .subscribe(
                data => {
                    this._router.navigate([this.returnUrl]);
                },
                error => {
                    console.log(error);
                    this.loading = false;
                });
    }

}
